---
title: "PSET 02"
date: 2020-07-02T17:46:06-05:00
## Use a standard weight of 10 so that posts can be more easily managed in the feature.
weight: 10
# aliases: ["/first"]
tags: ["migration", "work", "jobs"]
categories: ["work", "jobs"]
author: ["clcuc"]
showToc: true
TocOpen: true
#draft: true
hidemeta: false
comments: false
description: "Some problems of Complex Analysis"
#canonicalURL: "https://canonical.url/to/page"
disableHLJS: false# to disable highlightjs
disableShare: true
hideSummary: false
searchHidden: false
ShowReadingTime: true
ShowBreadCrumbs: true
ShowPostNavLinks: true
#cover:
#    image: "<image path/url>" # image path/url
#    alt: "<alt text>" # alt text
#    caption: "<text>" # display caption under cover
#    relative: false # when using page bundles set this to true
#    hidden: true # only hide on current single page
#editPost:
#    URL: "https://github.com/<path_to_repo>/content"
#    Text: "Suggest Changes" # edit text
#    appendFilePath: true # to append file path to Edit link
---

## Problem 01

Let $z\in \mathbb{C}$. Find $\lim_\limits{n\to \infty}{n{z^n}}$ when: $|z|<1$ .

### Solution

If $z=0$ there is nothing to do.

Otherwise, ${1 \over |z|} > 1$ and from
the first 3 terms of the binomial expansion we get
${1 \over |z|^n} = (1+({1 \over |z|}-1))^n \ge 1 + n ({1 \over |z|}-1) + { 1\over 2} n(n-1) ({1 \over |z|}-1)^2$, and
so
$n|z|^n \le {n \over 1 + n ({1 \over |z|}-1) + { 1\over 2} n(n-1) ({1 \over |z|}-1)^2}$. Since the right hand side $\to 0$, we have the desired result.

<!-- If you want to get the source code, then replace questions or letter a with posts and then add revisions at the end. -->
<!-- P01: https://math.stackexchange.com/questions/1571231/find-limit-for-ncn-when-c1 -->
<!-- Similar to P01:  https://math.stackexchange.com/questions/2981843/infty-cdot-0-indetermination-without-lhopital -->
<!-- P01: https://math.stackexchange.com/questions/226159/please-show-me-how-to-calculate-the-folowing-limit-of-a-sequence -->

## Problem 02

Find the limits as $n$ goes to infinity:  

$$\left( i+\left(\frac{2+3i}5\right)^n\right)_{n\in\mathbb N}$$    

$$\left( i+\left(\frac{3+4i}5\right)^n\right)_{n\in\mathbb N}$$

### Solution  

In the first case, the module of the second term is less than one, therefore the second term will go to zero, and thus the
whole expression will go to $i$.  

In the second case, the module of the second term is equal to one, therefore the second term will "rotate" indefinitely, and thus
the whole expression will not have a limit.  

<!-- Problem 02:  https://math.stackexchange.com/questions/3257419/exists-displaystyle-lim-n-rightarrow-infty-lefti-left-frac43i5-ri -->

## Problem 03  

Let $ (z_{n})_{n\in \mathbb{N}}$ be a sequence of complex numbers, such that the subsequences $(z_{2n})_{n\in \mathbb{N}}$ and 
$(z_{2n-1})_{n\in \mathbb{N}}$ are convergent. Prove that $(z_{n})_{n\in \mathbb{N}}$ is convergent. 

### Solution  

For every $\epsilon >0$,

$|z_{2n}-l|<\epsilon$ whenever $n>N_1$, and 

$|z_{2n+1}-l|<\epsilon$ whenever $n>N_2$.

Where the vertical bars represent the modulus of complex numbers.
Let $N=\max(2N_1+1,2N_2+2)$ and $n\gt N$. Then:

- either $n$ is even, thus $n=2k$ with $k\gt N_1$ (because $N\geqslant2N_1+1$ and $n\gt N$) hence $|z_n-\ell|=|z_{2k}-\ell|\lt\epsilon$,

- or $n$ is odd, thus $n=2k+1$ with $k\gt N_2$ (because $N\geqslant2N_2+2$ and $n\gt N$) hence $|z_n-\ell|=|z_{2k+1}-\ell|\lt\epsilon$.

This proves that, for every $n\gt N$, $|z_n-\ell|\lt\epsilon$.

<!-- https://math.stackexchange.com/questions/566841/z-2n-n-in-mathbbn-and-z-2n1-n-in-mathbbn-converges-to-sam -->

## Problem 04  

Let $ (z_{n})_{n\in \mathbb{N}}$ be a sequence of complex numbers that converge to $z_0$. Prove that:  

$$ \lim_{n \to \infty} \frac{z_1+z_2+\dots +z_n}{n} =z_0 $$

### Solution  

This concept is called the Cesaro mean. You can read 
[more about it in this article about Cesaro summation.](https://en.wikipedia.org/wiki/Ces%C3%A0ro_summation). 
The [Toeplitz theorem](https://en.wikipedia.org/wiki/Silverman%E2%80%93Toeplitz_theorem) might (or might not) be related.

<!-- https://math.stackexchange.com/questions/2352889/%c4%b0f-lim-x-n-x-then-how-to-show-this-equation -->

## Problem 05  

Let $ (z_{n})_{n\in \mathbb{N}}$ be a sequence of complex numbers such that $ \lim\limits_{n\to \infty} (z_{n+1}-z_n) =\lambda $.  

1. Prove that $\lim\limits_{n\to \infty} \frac{z_n}{n}=\lambda $.

2. Prove that $ \lim_{n \to \infty} \frac{z_1+z_2+\dots +z_n}{n^2} =\frac{\lambda}{2} $.

### Solution  

Item 1. Define $s_n :=z_n-z_{n-1}$ for $n\geq 2$, and $s_1=z_1$. We see that the limit of that sequence when $n$ goes to $\infty$ is
$\lambda$. THen we can apply the property of the Cesaro mean explained in the problem 04 above. That ends the proof.   

Item 2.  

Method 1: Apply [Stolz-Cesaro theorem](https://en.wikipedia.org/wiki/Stolz%E2%80%93Ces%C3%A0ro_theorem). Let $y_n :=n^2$. We can see
that the limit of $y_n$ as n goes to infinity is plus infinity. Then we can apply Stolz-Cesaro to ge the final result.   
Method 2: Let $\tau := \frac{\lambda}{2} $. Then, using the result of the item 1, we can prove that:  

$$ \lim_{n\to \infty} \frac{z_n}{2n-1}=\tau $$

We now remember that the sum of all odd numbers up to the n-th term is equal to $n^2$. And we define:  

$$F_n :=\lim_{n \to \infty} \frac{z_1+z_2+\dots +z_n}{n^2} -\tau =\frac{\sum\limits_{k=1}^{n} (2k-1)[\frac{z_k}{2k-1}-\tau ]}{n^2} $$

In a similar manner to problem 06, we "separate" appropriately this last sum into two parts. The first part has an maximum value of
the counter equal to a carefully chosen $n_0$, and the second part is the rest. The first part is contant and will converge to zero
when $n\to 0$. And we can prove that the second part will converge to zero using the convergence shown in the first lines of this
method. That ends the proof.  

## Problem 06  

$ \newcommand{\N}{\mathbb{N}} $

Let $ (u_{n})_{n \in \N } $ and $ (v_n)_{n \in \N } $ be two sequences of real numbers convergents to zero. Suppose that there exists a
number $M>0$ such that

\begin{equation}
\label{q06c01}
	\forall n \in \N ,  \mid v_0 \mid +\mid v_1\mid+\ldots +\mid v_n\mid \leq M \quad .
\end{equation}

Prove that
\begin{equation}
\label{q06c02}
	\lim_{n\to \infty}u_0v_n+u_1v_{n-1}+\ldots +u_nv_0=0 \quad .
\end{equation}

### Solution:  

In this case the strong condition is $\sum |v_n| \le M < \infty$ .
And the weak condition is ($u_n \to 0$).

Let $x_n=u_0v_n+u_1v_{n-1}+\ldots +u_nv_0$

Let $C>0$ and $ |u_m| <C$ for $ m \ge 0$ (since $u_m$ is convergent, thus it is bounded).

Let $\epsilon >0$ arbitrary and using the strong condition, we say: there exists $N(\epsilon)$ such that $, \sum_{n \ge N(\epsilon)} |v_n| < \epsilon /(2C)$ which means that the part of $x_Q$ that has $v$'s from $N(\epsilon)$ to $Q$ is at most $\epsilon /2$ for any $Q > N(\epsilon)$

However, the rest of the terms in $x_Q$ are only $N(\epsilon)$ in number and that is now fixed for given $\epsilon$; also the $u$'s indices are high for each of them (they go from $Q-N(\epsilon)+1$ to $Q$).

It follows then, that if we pick $Q_1(\epsilon)$ such that $, |u_Q| \le \epsilon/(2N(\epsilon)M)$ whenever $Q >Q_1(\epsilon)$; and
then $Q(\epsilon)$ a bit higher to ensure that $Q>Q(\epsilon)$ implies $Q-N(\epsilon)+1 > Q_1(\epsilon)$ ( so for example
$Q(\epsilon)=Q_1(\epsilon)+N(\epsilon)$ will suffice), the remaining terms in $x_Q$ are at most $\epsilon/(2N(\epsilon))$ so they
sum to at most $\epsilon /2$ making the full $|x_Q| \le \epsilon, Q \ge Q(\epsilon)$.

Given that $\epsilon >0$ arbitrary, we get $x_Q \to 0, Q \to \infty$ so that ends the proof.  
